#include "archive.hpp"

using namespace omicron;

Archive::Archive(const std::filesystem::path &path) : m_path{ path } {}

Archive::~Archive() noexcept {
    std::filesystem::remove_all(tmp_path());
}

auto Archive::deflate() -> void {
    std::filesystem::remove_all(tmp_path());
    std::filesystem::create_directory(tmp_path());

    // Extract the contents of the archive into a temporary directory.
    const auto tar_command = "tar -xzf " + std::string(m_path) + " -C " + tmp_path();
    std::system(tar_command.c_str());

    // Create a record for the file.
    for (const auto &file : std::filesystem::directory_iterator(tmp_path())) {
        m_files.emplace_back(file.path());
    }

    // Sort file names alphabetically.
    std::sort(m_files.begin(), m_files.end(), [](File &a, File &b) {
        return a.name() < b.name();
    });
}
