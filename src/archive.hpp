#pragma once

#include "file.hpp"
#include <filesystem>
#include <string>
#include <vector>

namespace omicron {

    // Represents a tar archive.
    struct Archive {
        Archive(const std::filesystem::path &path);
        ~Archive() noexcept;

        operator bool() const {
            return std::filesystem::exists(m_path);
        }

        auto name() const -> std::string {
            return m_path.stem();
        }

        auto tmp_path() const -> std::string {
            return "/tmp/" + name(); 
        }

        auto files() -> std::vector<File>& {
            return m_files;
        }

        // Extracts contents of the files into a temporary directory created under /tmp/ + file name.
        auto deflate() -> void;

        private:
            std::filesystem::path m_path;
            std::vector<File> m_files;
    };
}