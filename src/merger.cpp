#include "merger.hpp"
#include <vector>
#include <fstream>
#include <iostream>

using namespace omicron;

Merger::Merger(const std::vector<Archive> &archives, const std::filesystem::path &output) : m_archives{ archives }, m_output{ output } {}

auto Merger::merge() -> void {
    std::filesystem::remove_all(m_output);
    std::filesystem::create_directory(m_output);

    // Sort archive names alphabetically.
    std::sort(m_archives.begin(), m_archives.end(), [](Archive &a, Archive &b) {
        return a.name() < b.name();
    });

    // Expand all archives into a temporary directory so we can list all files then make a list of unique file names.
    std::vector<std::string> names;
    for (auto &archive : m_archives) {
        archive.deflate();
        for (auto &file : archive.files()) {
            if (std::find(names.begin(), names.end(), file.name()) == names.end()) {
                names.push_back(file.name());
            }
        }
    }

    // Because we're addicted to sorting and it helps with debugging.
    std::sort(names.begin(), names.end(), [](std::string &a, std::string &b) {
        return a < b;
    });

    // Start merging files.
    for (const auto &name : names) {
        bool first_file = true;
        for (auto &archive : m_archives) {
            for (auto &file : archive.files()) {
                if (file.name() == name) {
                    const auto output_file = m_output / name;
                    // Simply copy the first file.
                    if (first_file) {
                        std::filesystem::copy(file.path(), output_file);
                        first_file = false;
                    } else {
                        // For all remaining files do a little parsing.
                        std::ofstream destination_file(output_file, std::ios_base::binary | std::ios_base::app);
                        destination_file.seekp(0, std::ios_base::end);

                        std::ifstream source_file(file.path(), std::ios_base::binary);
                        std::string line;
                        bool first_line = true;
                        while(getline(source_file, line)) {
                            if (first_line) {
                                first_line = false;
                                continue;
                            }
                            destination_file << line << std::endl;
                        }
                    }
                }
            }
        }
    }
}
