#pragma once

#include <filesystem>
#include <string>

namespace omicron {

    // Represents a file in the filesystem.
    struct File {
        File(const std::filesystem::path &path) : m_path{ path } {}
        ~File() = default;

        auto name() const -> std::string {
            return m_path.stem();
        }

        auto path() const -> std::string {
            return m_path;
        }

        inline bool operator == (const std::string& n) {
            return name() == n;
        }

        private:
            std::filesystem::path m_path;
    };
}
