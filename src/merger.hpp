#pragma once

#include "archive.hpp"
#include "file.hpp"
#include <filesystem>
#include <vector>

namespace omicron {

    // Represents one merge operation for several log files.
    struct Merger {
        Merger(const std::vector<Archive> &archives, const std::filesystem::path &output);
        ~Merger() = default;

        // Merge all files as per spec.
        auto merge() -> void;

        private:
            // Input files
            std::vector<Archive> m_archives;
            // Output folder
            std::filesystem::path m_output;
    };
}
