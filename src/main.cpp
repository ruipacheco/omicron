#include "merger.hpp"
#include "archive.hpp"
#include "file.hpp"
#include <vector>
#include <filesystem>
#include <iostream>

using namespace omicron;

auto main(int argc, char** argv) -> int {
    std::vector<Archive> archives;
    std::filesystem::path output;
    for (auto i = 1; i < argc; ++i) {
        const std::filesystem::path tmp = argv[i];
        if (tmp.extension() == ".tgz") {
            const Archive archive{ tmp };
            archives.push_back(archive);
        } else {
            output = tmp;
            break;
        }
    }
    Merger merger{ archives, output };
    merger.merge();
}
